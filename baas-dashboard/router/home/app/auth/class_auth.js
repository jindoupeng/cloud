/** 权限**/
const router = require("koa-router")();
/**
 * api {get} /home/app/auth/class/auth 权限列表
 *
 *
 */
router.get("/class/auth", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;

  const classAuth = await BaaS.Models.class_auth
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: ["class", "auth"]
    });

  ctx.success(classAuth, "权限列表");
});
/**
 * api {get} /home/app/auth/info/class/auth 权限额外信息
 *
 *
 */
router.get("/info/class/auth", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;

  // 分组
  const group = await BaaS.Models.class
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: []
    });
  // 秘钥
  const auth = await BaaS.Models.auth
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: []
    });

  const data = {
    group: group,
    auth: auth
  };

  ctx.success(data, "权限额外信息");
});

/**
 * api {get} /home/app/auth/class/auth/:id 获取单个权限
 *
 * apiParam {Number} id
 *
 */
router.get("/class/auth/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const classAuth = await BaaS.Models.class_auth
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch();

  ctx.success(classAuth, "单个权限");
});
/**
 * api {post} /home/app/auth/add/class/auth 新增修改权限
 * 
 * apiParam {
		id: id,
		class_id: class_id,
		auth_id: auth_id
	} 
 * 
 */
router.post("/add/class/auth", async (ctx, nex) => {
  const baas = ctx.baas;
  const id = ctx.post.id;
  const classId = ctx.post.class_id;
  const authId = ctx.post.auth_id;

  const classAuth = await BaaS.Models.class_auth
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.where("class_id", "=", classId);
      qb.where("auth_id", "=", authId);
    })
    .fetch();
  if (classAuth && classAuth.id != id) {
    ctx.error("", "此分组权限已存在");
    return;
  }

  const result = await BaaS.Models.class_auth
    .forge({
      id: id,
      baas_id: baas.id,
      class_id: classId,
      auth_id: authId
    })
    .save();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:class:*`
  );

  ctx.success(result, "保存成功");
});
/**
 * api {get} /home/app/auth/del/class/auth/:id 删除权限
 *
 * apiParam {Number} id
 *
 */
router.get("/del/class/auth/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const classAuth = await BaaS.Models.class_auth
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch();
  if (!classAuth) {
    ctx.error("", "参数错误");
    return;
  }

  const result = await BaaS.Models.class_auth
    .forge({
      id: id
    })
    .destroy();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:class:*`
  );

  ctx.success(result, "删除权限");
});

module.exports = router;
